Generate the bundle in production mode.
```
$ webpack -p
```
NODE_ENV to production
```
NODE_ENV=production
```

Run a simple static file web server.
```
http-server src/static
```
Generate server.js
```
babel-node --presets 'react,es2015' src/server.js
```
or
```
node_modules/.bin/babel-node --presets 'react,es2015' src/server.js
```
or
```
NODE_ENV=production node_modules/.bin/babel-node --presets 'react,es2015' src/server.js
```


References:
https://github.com/lmammino/judo-heroes-2
